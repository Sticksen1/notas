package br.ifsc.edu.notas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class NotasDAO {

    SQLiteDatabase bd;

    public NotasDAO(Context c) {
        bd = c.openOrCreateDatabase("meubd", c.MODE_PRIVATE, null);

        bd.execSQL("CREATE TABLE IF NOT EXISTS notas (" +
                "id integer primary key autoincrement,"
                + " titulo varchar not null,"
                + "texto varchar);");

//        bd.execSQL("INSERT INTO notas (id,titulo,texto) VALUES (null, " +
//                " \" Felicidade\", + \"Ola \")");

        ContentValues contentValues = new ContentValues();
        contentValues.put("titulo" ,"Agora estou feliz!");
        contentValues.put("texto", "Hoje eu to um pouco feliz!");

        bd.insert("notas", null, contentValues);
    }

    public ArrayList<Nota> getNotas(){
        ArrayList<Nota> arrayListRsult = new ArrayList<Nota>();

        Cursor cursor = bd.rawQuery("SELECT * FROM notas",null,null);
        cursor.moveToFirst();

        String titulo, texto;
        int id;


        while (!cursor.isAfterLast()){

            id = cursor.getInt(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
            texto = cursor.getString(cursor.getColumnIndex("texto"));
            arrayListRsult.add(new Nota(id, titulo, texto));

            //Log.d("TabelaNotas", id + titulo + texto);

            cursor.moveToNext();

        }

      return arrayListRsult;
    };

}
